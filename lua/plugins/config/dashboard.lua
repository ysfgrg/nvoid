local g = vim.g

local plugins_count = vim.fn.len(vim.fn.globpath("~/.local/share/nvim/site/pack/packer/start", "*", 0, 1))
g.dashboard_default_executive = "telescope"

vim.cmd([[
let g:dashboard_custom_header =<< trim END
███╗   ██╗██╗   ██╗ ██████╗ ██╗██████╗ 
████╗  ██║██║   ██║██╔═══██╗██║██╔══██╗
██╔██╗ ██║██║   ██║██║   ██║██║██║  ██║
██║╚██╗██║╚██╗ ██╔╝██║   ██║██║██║  ██║
██║ ╚████║ ╚████╔╝ ╚██████╔╝██║██████╔╝
╚═╝  ╚═══╝  ╚═══╝   ╚═════╝ ╚═╝╚═════╝ 
END
]])


g.dashboard_disable_statusline = 0

g.dashboard_custom_footer = {
   "   ",
   "   ",
  "NVOID Loaded " .. plugins_count .. " plugins",
}

g.dashboard_custom_section = {
   a = { description = { "  Find File                 SPC f f" }, command = "Telescope find_files" },
   c = { description = { "  Recents                   SPC f h" }, command = "Telescope oldfiles" },
   d = { description = { "  Colorschemes              SPC f c" }, command = "Telescope colorscheme" },
   e = { description = { "  New File                  SPC f n" }, command = "DashboardNewFile" },
   f = { description = { "煉 Settings                  SPC f e" }, command = "e ~/.config/nvim/lua/settings.lua" },
   g = { description = { "  Load Last Session         SPC s l" }, command = "SessionLoad" },
}
