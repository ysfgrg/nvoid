local M = {}
vim.cmd("colorscheme onedarker")
M.set_statusbar = require("colors.status").onedarker
M.onedark = require("colors.onedarker.colors")
M.nord = require("colors.nord.colors")
return M
