local M = {}
M.onedarker = require("colors.onedarker.colors")
M.nord = require("colors.nord.colors")
return M
