local opt = vim.opt
local g = vim.g
local cmd = vim.cmd

opt.mouse = 'a'
opt.mouse = 'nicr'
opt.clipboard = 'unnamedplus'
opt.completeopt = "menuone,noselect"

opt.history = 100
opt.cmdheight = 2
opt.synmaxcol = 240
opt.shiftwidth = 4
opt.tabstop = 4
opt.pumheight = 10
opt.timeoutlen = 150

opt.cursorline = true
opt.splitright = true
opt.number = true
opt.relativenumber = true
opt.showmatch = true
opt.lazyredraw = true
opt.hidden = true
opt.expandtab = true
opt.splitbelow = true
opt.ignorecase = true
opt.smartcase = true
opt.smartindent = true
opt.swapfile = false
opt.backup = false
opt.showmode = false
opt.wrap = false
opt.termguicolors = true

g.transparent_background = false
g.italic_comments = true
g.italic_keywords = true
g.italic_functions = true
g.italic_variables = true
g.rnvimr_enable_picker = 1
g.matchup_matchparen_stopline = 400
g.matchup_delim_stopline = 1500
g.loaded_matchit = 1

cmd("let g:matchup_matchparen_offscreen = {'method': 'popup'}")
cmd("set shortmess+=c")
cmd("let g:loaded_matchit = 1")
