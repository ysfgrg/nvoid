<div align="center">

```
███╗   ██╗██╗   ██╗ ██████╗ ██╗██████╗ 
████╗  ██║██║   ██║██╔═══██╗██║██╔══██╗
██╔██╗ ██║██║   ██║██║   ██║██║██║  ██║
██║╚██╗██║╚██╗ ██╔╝██║   ██║██║██║  ██║
██║ ╚████║ ╚████╔╝ ╚██████╔╝██║██████╔╝
╚═╝  ╚═══╝  ╚═══╝   ╚═════╝ ╚═╝╚═════╝ 
```

[![Neovim Minimum Version](https://img.shields.io/badge/Neovim-0.5+-blueviolet.svg?style=flat-square&logo=Neovim&logoColor=white)](https://github.com/neovim/neovim)
[![LICENSE](https://img.shields.io/gitlab/license/ysfgrg/nvoid?style=flat-square&logo=GNU&label=License)](https://gitlab.com/ysfgrg/nvoid/-/blob/main/LICENSE)
![1](https://gitlab.com/ysfgrg/nvoid/-/raw/main/scrot1.png)
[![Lua](https://img.shields.io/badge/Made%20with%20Lua-blue.svg?style=for-the-badge&logo=lua)](https://lua.org)
</div>

# Install In One Command
```
bash -c "$(curl -s https://gitlab.com/ysfgrg/nvoid/-/raw/main/NVOID-Install.sh)"
```


# Install LSP
+ it comes by default with <Lua,vim,bash> languages
```
:LspInstall <Language name>
```

# Thanks To
+ [Lunarvim](https://github.com/LunarVim/LunarVim)
+ [Nvchad](https://github.com/NvChad/NvChad)
+ [CosmicNvim](https://github.com/mattleong/CosmicNvim)
+ [Nv-ide](https://github.com/crivotz/nv-ide)
+ [Kyoto.nvim](https://github.com/samrath2007/kyoto.nvim) 
